FROM node:11.1.0-alpine
COPY . /app 
COPY public/ ./app/public/
WORKDIR /app 
RUN npm install --production
EXPOSE 1339
ENTRYPOINT [ "npm", "run"]
CMD [ "start", "--prod"]