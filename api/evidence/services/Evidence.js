/* global Evidence */
'use strict';

/**
 * Evidence.js service
 *
 * @description: A set of functions similar to controller's actions to avoid code duplication.
 */

// Public dependencies.
const _ = require('lodash');
const axios = require('axios');
const fs = require('fs');

// Strapi utilities.
const utils = require('strapi-hook-bookshelf/lib/utils/');
const { convertRestQueryParams, buildQuery } = require('strapi-utils');


module.exports = {

  /**
   * Promise to fetch all evidence.
   *
   * @return {Promise}
   */

  fetchAll: (params, populate) => {
    // Select field to populate.
    const withRelated = populate || Evidence.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias);

    const filters = convertRestQueryParams(params);

    return Evidence.query(buildQuery({ model: Evidence, filters }))
      .fetchAll({ withRelated })
      .then(data => data.toJSON());
  },

  /**
   * Promise to fetch a/an evidence.
   *
   * @return {Promise}
   */

  fetch: (params) => {
    // Select field to populate.
    const populate = Evidence.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias);

    return Evidence.forge(_.pick(params, 'id')).fetch({
      withRelated: populate
    });
  },

  /**
   * Promise to count a/an evidence.
   *
   * @return {Promise}
   */

  count: (params) => {
    // Convert `params` object to filters compatible with Bookshelf.
    const filters = convertRestQueryParams(params);

    return Evidence.query(buildQuery({ model: Evidence, filters: _.pick(filters, 'where') })).count();
  },

  telegram2: async (params, populate) => {
    // KEY
    const key = '886141367:AAFkMc7n6bWm-rwk5T_1-dGs_g017hVCzLQ';
    const file_id = 'AgADBQAD26gxGyZY2VSpFH1LoAx18IK1-TIABAEAAwIAA20AAwMvBAABFgQ';

    var check = await axios.get('https://api.telegram.org/bot' + key + '/getFile?file_id=' + file_id)
      .then(async response => {
        let res = response.data.result;

        let urlPath = 'https://api.telegram.org/file/bot' + key + '/' + res.file_path;
        let ext = urlPath.split('.').pop();
        let localPath = './public/downloads/' + Date.now() + '' + 1 + '.' + ext;
        
        await axios({
          method: 'get',
          url: urlPath,
          responseType:'stream'
          })
        .then(res => {
          res.data.pipe(fs.createWriteStream(localPath));
        })
        .catch(err => console.log(err));

      })
      .catch(error => {
        return error;
      });

    return check;
  },

  telegram: async (params, populate) => {

    const withRelated = populate || Evidence.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias);

      const json_evidence = {
        is_deleted: false,
        is_public: true,
        dari: 'telegram',
        _sort: 'batch:desc,id_telegram:desc',
        _limit: 1
      };

    const filters = convertRestQueryParams(json_evidence);

    const check = await Evidence.query(buildQuery({ model: Evidence, filters }))
      .fetchAll({ withRelated })
      .then(data => {return data.toJSON()});

    let id_telegram_old = 0;
    let batch = 0;
    let offset = '';
    if(check[0]){
      id_telegram_old = check[0].id_telegram;
      batch = Number(check[0].batch) + 1;
      offset = '?offset=' + id_telegram_old;
    }

    // return offset + ' ' + batch;

    const key = '886141367:AAFkMc7n6bWm-rwk5T_1-dGs_g017hVCzLQ';
    return await axios.get('https://api.telegram.org/bot' + key + '/getUpdates' + offset)
      .then(async response => {
        let data = response.data.result;
        let length = response.data.result.length;
        // let length = 2;

        let media_group_id = '';
        let caption = '';
        let hasil = [];
        let hasil_caption = [];
        for (var i = 0; i < length; i++) {
          // DATA
          if(data[i].hasOwnProperty('message')){
            let arr = data[i].message;
            let id_telegram = data[i].update_id;
            if(arr.hasOwnProperty('forward_from')){
              var user_telegram = arr.forward_from.id;
              var last_name = (arr.forward_from.last_name) ? ' ' + arr.forward_from.last_name : '';
              var nama_telegram = arr.forward_from.first_name + last_name;
              var date = arr.forward_date;
            } else {
              var user_telegram = arr.from.id;
              var last_name = (arr.from.last_name) ? ' ' + arr.from.last_name : '';
              var nama_telegram = arr.from.first_name + last_name;
              var date = arr.date;
            }

            // return arr;
            // hasil.push(nama_telegram);

            if(Number(id_telegram) > Number(id_telegram_old)){
              // Jika Photo Group
              if(arr.hasOwnProperty('media_group_id')){
                if(media_group_id == ''){
                  media_group_id = arr.media_group_id;
                  if(arr.hasOwnProperty('caption')){
                    caption = arr.caption;
                  } else {
                    caption = '';
                  }
                } else if(media_group_id == arr.media_group_id){
                  media_group_id = arr.media_group_id;
                  caption = caption;
                } else if(media_group_id != arr.media_group_id){
                  media_group_id = arr.media_group_id;
                  if(arr.hasOwnProperty('caption')){
                    caption = arr.caption;
                  } else {
                    caption = '';
                  }
                } else {
                  caption = '';
                }
              } else {
                if(arr.hasOwnProperty('caption')){
                  caption = arr.caption;
                } else {
                  caption = '';
                }
              }

              // hasil.push(caption);

              // Jika Ada Photo
              if(arr.hasOwnProperty('photo')){
                let last_element = arr.photo[arr.photo.length - 1];
                let file_id = last_element.file_id;
                // hasil.push(file_id);
                await axios.get('https://api.telegram.org/bot' + key + '/getFile?file_id=' + file_id)
                  .then(async response => {

                    let res = response.data.result;

                    let urlPath = 'https://api.telegram.org/file/bot' + key + '/' + res.file_path;
                    let ext = urlPath.split('.').pop();
                    let fileName = Date.now() + '' + i + '.' + ext;
                    let localPath = './public/uploads/' + fileName;
                    let hasilPath = '/uploads/' + fileName;
                    
                    await axios({
                      method: 'get',
                      url: urlPath,
                      responseType:'stream'
                      })
                    .then(res => {
                      res.data.pipe(fs.createWriteStream(localPath));
                    })
                    .catch(err => console.log(err));

                    let nama = res.file_id;
                    let path = hasilPath;
                    let json_evidence = {
                      nama: nama,
                      path: path,
                      // keterangan: key,
                      tag: caption,
                      id_telegram: id_telegram,
                      user_telegram: user_telegram,
                      nama_telegram: nama_telegram,
                      batch: batch,
                      is_deleted: false,
                      is_public: true,
                      dari: 'telegram',
                      created_at: new Date(Number(date) * 1000),
                      updated_at: new Date(Number(date) * 1000)
                    };
                    // hasil.push(json_evidence);

                    const relations = _.pick(json_evidence, Evidence.associations.map(ast => ast.alias));
                    const data = _.omit(json_evidence, Evidence.associations.map(ast => ast.alias));

                    const entry = await Evidence.forge(data).save();
                    hasil.push(entry.id);
                    // hasil.push(i);
                  })
                  .catch(error => {
                    return error;
                  });
              }
            }
          }
        }
        // return hasil_caption;
        return hasil;
      })
      .catch(error => {
        return error;
      });
  },

  /**
   * Promise to add a/an evidence.
   *
   * @return {Promise}
   */

  add: async (values) => {
    // Extract values related to relational data.
    const relations = _.pick(values, Evidence.associations.map(ast => ast.alias));
    const data = _.omit(values, Evidence.associations.map(ast => ast.alias));

    // Create entry with no-relational data.
    const entry = await Evidence.forge(data).save();

    // Create relational data and return the entry.
    return Evidence.updateRelations({ id: entry.id , values: relations });
  },

  /**
   * Promise to edit a/an evidence.
   *
   * @return {Promise}
   */

  edit: async (params, values) => {
    // Extract values related to relational data.
    const relations = _.pick(values, Evidence.associations.map(ast => ast.alias));
    const data = _.omit(values, Evidence.associations.map(ast => ast.alias));

    // Create entry with no-relational data.
    const entry = await Evidence.forge(params).save(data);

    // Create relational data and return the entry.
    return Evidence.updateRelations(Object.assign(params, { values: relations }));
  },

  /**
   * Promise to remove a/an evidence.
   *
   * @return {Promise}
   */

  remove: async (params) => {
    params.values = {};
    Evidence.associations.map(association => {
      switch (association.nature) {
        case 'oneWay':
        case 'oneToOne':
        case 'manyToOne':
        case 'oneToManyMorph':
          params.values[association.alias] = null;
          break;
        case 'oneToMany':
        case 'manyToMany':
        case 'manyToManyMorph':
          params.values[association.alias] = [];
          break;
        default:
      }
    });

    await Evidence.updateRelations(params);

    return Evidence.forge(params).destroy();
  },

  /**
   * Promise to search a/an evidence.
   *
   * @return {Promise}
   */

  search: async (params) => {
    // Convert `params` object to filters compatible with Bookshelf.
    const filters = strapi.utils.models.convertParams('evidence', params);
    // Select field to populate.
    const populate = Evidence.associations
      .filter(ast => ast.autoPopulate !== false)
      .map(ast => ast.alias);

    const associations = Evidence.associations.map(x => x.alias);
    const searchText = Object.keys(Evidence._attributes)
      .filter(attribute => attribute !== Evidence.primaryKey && !associations.includes(attribute))
      .filter(attribute => ['string', 'text'].includes(Evidence._attributes[attribute].type));

    const searchInt = Object.keys(Evidence._attributes)
      .filter(attribute => attribute !== Evidence.primaryKey && !associations.includes(attribute))
      .filter(attribute => ['integer', 'decimal', 'float'].includes(Evidence._attributes[attribute].type));

    const searchBool = Object.keys(Evidence._attributes)
      .filter(attribute => attribute !== Evidence.primaryKey && !associations.includes(attribute))
      .filter(attribute => ['boolean'].includes(Evidence._attributes[attribute].type));

    const query = (params._q || '').replace(/[^a-zA-Z0-9.-\s]+/g, '');

    return Evidence.query(qb => {
      if (!_.isNaN(_.toNumber(query))) {
        searchInt.forEach(attribute => {
          qb.orWhereRaw(`${attribute} = ${_.toNumber(query)}`);
        });
      }

      if (query === 'true' || query === 'false') {
        searchBool.forEach(attribute => {
          qb.orWhereRaw(`${attribute} = ${_.toNumber(query === 'true')}`);
        });
      }

      // Search in columns with text using index.
      switch (Evidence.client) {
        case 'mysql':
          qb.orWhereRaw(`MATCH(${searchText.join(',')}) AGAINST(? IN BOOLEAN MODE)`, `*${query}*`);
          break;
        case 'pg': {
          const searchQuery = searchText.map(attribute =>
            _.toLower(attribute) === attribute
              ? `to_tsvector(${attribute})`
              : `to_tsvector('${attribute}')`
          );

          qb.orWhereRaw(`${searchQuery.join(' || ')} @@ to_tsquery(?)`, query);
          break;
        }
      }

      if (filters.sort) {
        qb.orderBy(filters.sort.key, filters.sort.order);
      }

      if (filters.skip) {
        qb.offset(_.toNumber(filters.skip));
      }

      if (filters.limit) {
        qb.limit(_.toNumber(filters.limit));
      }
    }).fetchAll({
      withRelated: populate
    });
  }
};
