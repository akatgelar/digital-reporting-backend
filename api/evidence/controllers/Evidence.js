'use strict';

/**
 * Evidence.js controller
 *
 * @description: A set of functions called "actions" for managing `Evidence`.
 */

module.exports = {

  /**
   * Retrieve evidence records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.evidence.search(ctx.query);
    } else {
      return strapi.services.evidence.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a evidence record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.evidence.fetch(ctx.params);
  },

  /**
   * Count evidence records.
   *
   * @return {Number}
   */

  count: async (ctx, next, { populate } = {}) => {
    return strapi.services.evidence.count(ctx.query, populate);
  },

  telegram: async (ctx, next, { populate } = {}) => {
    return strapi.services.evidence.telegram(ctx.query, populate);
  },

  /**
   * Create a/an evidence record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.evidence.add(ctx.request.body);
  },

  /**
   * Update a/an evidence record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.evidence.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an evidence record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.evidence.remove(ctx.params);
  }
};
