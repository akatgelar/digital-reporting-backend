'use strict';

/**
 * Dinas.js controller
 *
 * @description: A set of functions called "actions" for managing `Dinas`.
 */

module.exports = {

  /**
   * Retrieve dinas records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.dinas.search(ctx.query);
    } else {
      return strapi.services.dinas.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a dinas record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.dinas.fetch(ctx.params);
  },

  /**
   * Count dinas records.
   *
   * @return {Number}
   */

  count: async (ctx, next, { populate } = {}) => {
    return strapi.services.dinas.count(ctx.query, populate);
  },

  /**
   * Create a/an dinas record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.dinas.add(ctx.request.body);
  },

  /**
   * Update a/an dinas record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.dinas.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an dinas record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.dinas.remove(ctx.params);
  }
};
