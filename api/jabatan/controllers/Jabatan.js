'use strict';

/**
 * Jabatan.js controller
 *
 * @description: A set of functions called "actions" for managing `Jabatan`.
 */

module.exports = {

  /**
   * Retrieve jabatan records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.jabatan.search(ctx.query);
    } else {
      return strapi.services.jabatan.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a jabatan record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.jabatan.fetch(ctx.params);
  },

  /**
   * Count jabatan records.
   *
   * @return {Number}
   */

  count: async (ctx, next, { populate } = {}) => {
    return strapi.services.jabatan.count(ctx.query, populate);
  },

  /**
   * Create a/an jabatan record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.jabatan.add(ctx.request.body);
  },

  /**
   * Update a/an jabatan record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.jabatan.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an jabatan record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.jabatan.remove(ctx.params);
  }
};
