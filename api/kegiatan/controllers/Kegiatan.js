'use strict';

/**
 * Kegiatan.js controller
 *
 * @description: A set of functions called "actions" for managing `Kegiatan`.
 */

module.exports = {

  /**
   * Retrieve kegiatan records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.kegiatan.search(ctx.query);
    } else {
      return strapi.services.kegiatan.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a kegiatan record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.kegiatan.fetch(ctx.params);
  },

  /**
   * Count kegiatan records.
   *
   * @return {Number}
   */

  count: async (ctx, next, { populate } = {}) => {
    return strapi.services.kegiatan.count(ctx.query, populate);
  },

  /**
   * Create a/an kegiatan record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.kegiatan.add(ctx.request.body);
  },

  /**
   * Update a/an kegiatan record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.kegiatan.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an kegiatan record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.kegiatan.remove(ctx.params);
  }
};
