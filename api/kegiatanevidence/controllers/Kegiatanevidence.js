'use strict';

/**
 * Kegiatanevidence.js controller
 *
 * @description: A set of functions called "actions" for managing `Kegiatanevidence`.
 */

module.exports = {

  /**
   * Retrieve kegiatanevidence records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.kegiatanevidence.search(ctx.query);
    } else {
      return strapi.services.kegiatanevidence.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a kegiatanevidence record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.kegiatanevidence.fetch(ctx.params);
  },

  /**
   * Count kegiatanevidence records.
   *
   * @return {Number}
   */

  count: async (ctx, next, { populate } = {}) => {
    return strapi.services.kegiatanevidence.count(ctx.query, populate);
  },

  /**
   * Create a/an kegiatanevidence record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.kegiatanevidence.add(ctx.request.body);
  },

  /**
   * Update a/an kegiatanevidence record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.kegiatanevidence.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an kegiatanevidence record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.kegiatanevidence.remove(ctx.params);
  }
};
