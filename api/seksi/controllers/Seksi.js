'use strict';

/**
 * Seksi.js controller
 *
 * @description: A set of functions called "actions" for managing `Seksi`.
 */

module.exports = {

  /**
   * Retrieve seksi records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.seksi.search(ctx.query);
    } else {
      return strapi.services.seksi.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a seksi record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.seksi.fetch(ctx.params);
  },

  /**
   * Count seksi records.
   *
   * @return {Number}
   */

  count: async (ctx, next, { populate } = {}) => {
    return strapi.services.seksi.count(ctx.query, populate);
  },

  /**
   * Create a/an seksi record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.seksi.add(ctx.request.body);
  },

  /**
   * Update a/an seksi record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.seksi.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an seksi record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.seksi.remove(ctx.params);
  }
};
