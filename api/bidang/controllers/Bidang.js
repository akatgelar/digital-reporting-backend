'use strict';

/**
 * Bidang.js controller
 *
 * @description: A set of functions called "actions" for managing `Bidang`.
 */

module.exports = {

  /**
   * Retrieve bidang records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.bidang.search(ctx.query);
    } else {
      return strapi.services.bidang.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a bidang record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    return strapi.services.bidang.fetch(ctx.params);
  },

  /**
   * Count bidang records.
   *
   * @return {Number}
   */

  count: async (ctx, next, { populate } = {}) => {
    return strapi.services.bidang.count(ctx.query, populate);
  },

  /**
   * Create a/an bidang record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.bidang.add(ctx.request.body);
  },

  /**
   * Update a/an bidang record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.bidang.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an bidang record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.bidang.remove(ctx.params);
  }
};
